const app = new Vue({
    el : '#app',
    data: {
        items :[
            {name: 'Analisis', hh: 0, checked: false, opciones: {}, hijos: []},
            {name: 'Desarrollo', hh: 0, checked: false, opciones: {}, hijos: []},
            {name: 'Coordinacion Cliente', hh: 0, checked: false, opciones: {}, hijos: []},
            {name: 'Coordinacion Interna', hh: 0, checked: false, opciones: {}, hijos: []},
            {name: 'QA y Generacion de evidencias', hh: 0, checked: false, opciones: {}, hijos: []},
        ],
        tareas: [],
        subtotal : 0,
        GestionIntranet: 0,
        total: 0,
        SelectElementos: "",
        SelectElementosPadres:"",
        tarea:"",
        MsjsValidacion:[],
        elementos : ['Padre','Hijo'],
        muestra_panel:false,
        error:false,
        mostrar:false,
        Create:false,
        elemento:"",
        elementohijo:"",
        porcentaje:"",
        hh:0
    },
    computed: {
        errorsStyle() {
            return this.error ? 'alert alert-danger' : 'alert alert-success';
        },
    },
    methods: {
        Agregar : function(){
            this.mostrar = false;

            this.SelectElementos;
            this.SelectElementosPadres;
            this.tarea;
            this.validacionCampos();

            if(!this.error){
                if(this.SelectElementos == 0){
                    for (const index in  this.tareas) {
                        this.items.push({name: this.tareas[index].tarea, hh: this.tareas[index].hh, checked: false, opciones: {}, hijos: []});
                    }
                    this.MsjsValidacion.push("Elemento Creado");
                    this.limpiarFormularo();

                }else if(this.SelectElementos == 1) {
                    for (const index in  this.items) {
                        if( index == this.SelectElementosPadres ){
                            for (const index3 in  this.tareas) {
                                this.items[index].hijos.push({name: this.tareas[index3].tarea, hh: parseFloat(this.tareas[index3].hh), checked: false, opciones: {}});
                                this.items[index].hh = (parseFloat(this.tareas[index3].hh) + parseFloat(this.items[index].hh));
                            }
                            this.MsjsValidacion.push("Elemento Creado");
                            this.limpiarFormularo();
                        }
                    }
                }else {
                    this.error = true;
                    this.MsjsValidacion.push("ERROR!");
                }
            }
            this.mostrar = true;
           

        },
        Editar : function(){
            this.mostrar = false;

            this.SelectElementos;
            this.SelectElementosPadres;
            this.tarea;
            this.validacionCampos();

            if(!this.error){
                if(this.SelectElementos == 0){
                    for (const index in  this.tareas) {
                        this.items[this.elemento].name = this.tareas[index].tarea;
                        this.items[this.elemento].hh = parseFloat(this.tareas[index].hh);
                    }
                    this.limpiarFormularo();
                    this.MsjsValidacion.push("Elimento editado");

                }else if(this.SelectElementos == 1) {
                    for (const index in  this.tareas) {
                        this.items[this.elemento].hijos[this.elementohijo].name = this.tareas[index].tarea;
                        this.items[this.elemento].hijos[this.elementohijo].hh = parseFloat(this.tareas[index].hh);
                        this.items[this.elemento].hh =  (parseFloat(this.items[this.elemento].hh) + parseFloat(this.tareas[index].hh));
                    }
                    this.limpiarFormularo();
                    this.MsjsValidacion.push("Elimento editado");
                }else {
                    this.error = true;
                    this.MsjsValidacion.push("ERROR!");
                }
            }
            this.mostrar = true;
           

        },
        editarHijo : function(id_padre, id_hijo){
            /**Busca datos */
            this.tareas = [];
            this.muestra_panel = true;
            this.SelectElementos = '1';
            this.SelectElementosPadres = id_padre;
            this.tareas.push({
                tarea: this.items[id_padre].hijos[id_hijo].name,
                hh: parseFloat(this.items[id_padre].hijos[id_hijo].hh)
            });
            this.elemento = id_padre;
            this.elementohijo = id_hijo;
            this.Create = false;
        },
        editarPadre : function(id){
            /**Busca datos */
            this.tareas = [];
            this.muestra_panel = true;
            this.SelectElementos = '0';
            this.tareas.push({
                tarea: this.items[id].name,
                hh: parseFloat (this.items[id].hh)
            });

            this.elemento = id;
            this.Create = false;
        },
        eliminarHijo : function (id_padre,id) { 
            var opcion = confirm("Esta seguro que desea eliminar este elemento?");
            if (opcion == true) {
                for (const index in  this.items) {
                    if( index == id_padre ){
                        for (const index2 in  this.items[index].hijos) {
                            if( index2 == id ){
                                this.items[index].hijos.splice(index2,1);
                                this.actulizarHHPadre(id_padre);
                            }
                        }
                    }
                }
                this.MsjsValidacion = [];
                this.MsjsValidacion.push("Elemento Eliminado");
                this.mostrar= true;
            }          
            
        },
        eliminarPadre: function(id){
            var opcion = confirm("Esta seguro que desea eliminar este elemento?");
            if (opcion == true) {
                for (const index in  this.items) {
                    if( index == id ){ 
                        this.items.splice(index,1);
                    }
                }
                this.MsjsValidacion = [];
                this.MsjsValidacion.push("Elemento Eliminado");
                this.mostrar= true;

            } 
            
        },
        actulizarHHPadre: function(id_padre){
            var suma = 0;
            for (const index in  this.items) {
                if(index == id_padre ){
                    for (const key in this.items[index].hijos ) {
                        suma += parseFloat(this.items[index].hijos[key].hh);
                    }
                    this.items[index].hh = suma;
                }
            }
        },
        muestraPanel: function(muestra_panel){
            this.limpiarFormularo();
         
            if(this.muestra_panel &&  this.Create == false){
                this.Create = true;
                this.tareas = [];
                this.addElement();
            }else if(this.muestra_panel){
                this.muestra_panel = false;
                this.Create = false;
            }else {
                this.Create = true;
                this.muestra_panel = true;
                this.tareas = [];
                this.addElement();
            }
        },
        validacionCampos: function(){
        
            this.error = false;
            this.MsjsValidacion = [];
           

            if( this.SelectElementos === ""){
                this.MsjsValidacion.push(" ERROR! Debe Seleccionar un Elemento") ;
                this.error = true;
            }
            if(this.SelectElementos == "1"){

                if( this.SelectElementosPadres === ""){
                    this.MsjsValidacion.push(" ERROR! Debe Seleccionar un Elemento Padre") ;
                    this.error = true;
                }
            }
            for (const index in  this.tareas) {

                if( this.tareas[index].tarea == ""){
                    this.MsjsValidacion.push(" ERROR! Campo Titulo es requerido") ;
                    this.error = true;
                }

                if( this.tareas[index].hh == ""){
                    this.MsjsValidacion.push(" ERROR! Campo Titulo es requerido") ;
                    this.error = true;
                }
            }
            

        },
        limpiarFormularo: function(){
            this.tareas =[];
            this.SelectElementos =  "";
            this.SelectElementosPadres = "";
            this.tarea = "";
            this.muestra_panel = false;
            this.error = false;
            this.mostrar = false;
            this.elemento = "";
        },
        Calcular: function(){
            var suma = 0;
            for (const index in this.items) {
               suma += parseFloat (this.items[index].hh);
            }
            this.subtotal = suma;

            this.GestionIntranet = ((this.porcentaje / 100 ) * suma );
            this.total = this.GestionIntranet + suma;
        },
        ExporatEstimacion: function(){
            var contenido = "";
            var lineas = "==================================================================\n";
            var espacios = '\u00a0' + '\u00a0';
            contenido = lineas + "ESTIMACIÓN DE DETALLADA\n"+ lineas;

            //export txt detallado
            for (const index in this.items) {
                contenido += this.items[index].name + ': '+ this.items[index].hh +" HH \n";
               
                if(this.items[index].hijos.length > 0){
                    for (const index2 in this.items[index].hijos) {
                        contenido +=  espacios + this.items[index].hijos[index2].name + ': '+ this.items[index].hijos[index2].hh +" HH \n";
                    }
                }

            }

            contenido += "Subtotal: " + this.subtotal + " HH \n";
            contenido += "Gestion Intranet " + this.porcentaje + "% :"+ this.GestionIntranet+ " HH \n";
            contenido += "Tiempo Referencial Estimado: " + this.total +" HH \n";
            

            contenido += lineas + "ESTIMACIÓN DE CLIENTE\n"+ lineas;

            //export txt CLIENTE
            for (const index in this.items) {
                if(this.items[index].checked){
                    contenido += this.items[index].name + ': '+ this.items[index].hh +" HH \n";
                    if(this.items[index].hijos.length > 0){
                        for (const index2 in this.items[index].hijos) {
                            if(this.items[index].hijos[index2].checked){
                                contenido +=  espacios + this.items[index].hijos[index2].name + " \n";
                            }
                        }
                    }

                }
               

            }
            
            contenido += "Tiempo Referencial Estimado: " + this.total +" HH \n";

            var blob = new Blob([contenido], {type: "text/plain;charset=utf-8"});
            saveAs(blob, "Estimacion.txt");
        },
        addElement: function(){
            this.tareas.push({
                tarea: '',
                hh: '',
            });
        },
        deleteElement: function(id) {
            if(this.tareas.length > 1 ){
                var opcion = confirm("Esta seguro que desea eliminar este elemento?");
                if (opcion == true) {
                    this.tareas.splice(id,1);
                    this.MsjsValidacion = [];
                    this.MsjsValidacion.push("Elemento Eliminado");
                    this.mostrar= true;
                } 
            }
        }
    }
});
